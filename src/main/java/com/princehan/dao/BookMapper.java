package com.princehan.dao;

import com.princehan.pojo.Books;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Description
 * @Author:PrinceHan
 * @CreateTime:2022/4/19 12:57
 */
public interface BookMapper {
    int addBook(Books book);

    int deleteBookById(@Param("bookID") int id);

    int updateBook(Books book);

    Books queryBookById(@Param("bookID") int id);

    List<Books> queryAllBook();

    List<Books> queryBookByName(@Param("bookName") String name);

}
