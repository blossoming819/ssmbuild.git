<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="row clearfix">
        <div class="col-md-12 column">
            <div class="page-header">
                <h1>
                    <small>修改书籍</small>
                </h1>
            </div>
        </div>
    </div>
    <form action="${pageContext.request.contextPath}/book/updateBook" method="post">
        <input type="hidden" name="bookID" value="${book.bookID}">
        <div class="form-group">
            <label for="bookName">书籍名称</label>
            <input type="text" id="bookName" name="bookName" value="${book.bookName}" class="form-controller" required>
        </div>
        <div class="form-group">
            <label for="bookCounts">书籍数量</label>
            <input type="text" id="bookCounts" name="bookCounts" value="${book.bookCounts}" class="form-controller" required>
        </div>
        <div class="form-group">
            <label for="detail">书籍详情</label>
            <input type="text" id="detail" name="detail" value="${book.detail}" class="form-controller" required>
        </div>
        <div class="form-group">
            <input type="reset" value="重置">
            <input type="submit" value="提交">
        </div>
    </form>
</div>
</body>
</html>
